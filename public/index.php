<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <title>Fore! Eats - Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   
  <link rel="stylesheet" href="css/app.css" />
  <script src="bower_components/modernizr/modernizr.js"></script>
</head>
<body>

  
      <!-- <div class="row max-width-row" data-equalizer="deq-first">
        <div class ="contain-to-grid">
          <div class="medium-2 col-background column" data-equalizer-watch="deq-first">
          </div>
     <div class="medium-3 col-background column" data-equalizer-watch="deq-first">
         <div class="row1">
            <div class="medium-12 column">
              <img src="images/Cedarwood-look.png">
            </div>
         </div>
         <div class="row2">
            <div class="medium-12 column">
              <img src="images/ClearLakeGC-CourseOverviewLogowithWhiteBkgrd.png">
            </div>
         </div>
         <div class="row3">
            <div class="medium-12 column">
              <img src="images/Fortune-Bay-Bkgd.png">
            </div>
         </div>       
      </div>

     
      <div class="medium-5 col-background column end" data-equalizer-watch="deq-first">
        <div class="row">
          <div class="medium-6 col-contestmng-first column hide-for-small-only">
          </div>
          <div class="small-12 medium-6 col-contestmng-first column text-center">
            <img src="images/contest-mgr-logo.png">
          </div>
        </div>

        <div class="row">

          <div class="medium-3 col-contestmng-first column hide-for-small-only">
          </div>
         
          <div class="small-12 medium-9 col-contestmng-first column">
            <p id="pg-run"> RUN A</p>
            <p id="pg-contest">CONTEST</p>
            <p id="pg-connect">Connect with customers</p>
          </div>
          
        </div>
        
        <div class="row">
          <div class="medium-12 col-contestmng-first column text-center">
            <p class="pg-soliton">Soliton Apps</p>
          </div>
        </div>

        

      </div>
       <div class="medium-2 col-background column" data-equalizer-watch="deq-first">
        </div>
    </div> 
    </div>

   
  
   <div class="row max-width-row">
    <div class="medium-12 col-footer column text-center">
      <p class="footer-text">  Solitonapps.com/contestmanager    |    204.228.4785    |    info@solitonapps.com  </p>
    </div>
   </div> -->

  <div class="row max-width-row">

      
       <!--  <div class="medium-1 column" data-equalizer-watch="deq-first">
        </div>
        <div class="medium-4 column" data-equalizer-watch="deq-first" style="background: white;">
            <div class="row1">
              <div class="medium-12 column">
                <img src="images/Cedarwood-look.png">
              </div>
            </div>
            <div class="row2">
              <div class="medium-12 column">
                <img src="images/ClearLakeGC-CourseOverviewLogowithWhiteBkgrd.png">
              </div>
            </div>
            <div class="row3">
              <div class="medium-12 column">
                <img src="images/Fortune-Bay-Bkgd.png">
              </div>
            </div> 
        </div> -->
      
      <div class="medium-5 col-background column">
        <div class="row">
          <div class="medium-2 column">
          </div>
          <div class="medium-10 column" style="background: white">
              <div class="row1">
                <div class="medium-12 column">
                  <img src="images/Cedarwood-look.png">
                </div>
              </div>
              <div class="row2">
                <div class="medium-12 column">
                  <img src="images/ClearLakeGC-CourseOverviewLogowithWhiteBkgrd.png">
                </div>
              </div>
              <div class="row3">
                <div class="medium-12 column">
                  <img src="images/Fortune-Bay-Bkgd.png">
                </div>
              </div> 
          </div>
        </div>
      </div>

      <div class="medium-7 col-background column">
        <div class="row1" data-equalizer="hd-logo">
          <div class="medium-4 column" data-equalizer-watch="hd-logo">
            <img src="images/contest-mgr-logo.png">
          </div>
          <div class="medium-8 col-margin-top text-center column hide-for-small-only" data-equalizer-watch="hd-logo">
            <p class="large-text">CONTEST MANAGER</p>
          </div>
        </div>
        <div class="row2">
          <div class="medium-12 col-margin-top column">
            <p class="large-text">RUN A</p>
            <p class="large-text text-center">CONTEST</p>
            <p class="small-text text-center">Connect with customers</p>
          </div>
        </div>
      
        <div class="row3">
          <div class="medium-12 col-margin-top column">
            <p class="small-text">GREAT FOR:</p> 
            <ul style="list-style-type:disc">
              <li class="list-item">TRADE SHOWS</li>
              <li class="list-item">GOLF COURSES</li>
              <li class="list-item">CORPORATE EVENTS</li>
            </ul>
          </div>
        </div>  
      </div>
  </div>
       
     
      <!-- <div class="medium-5 col-background column end" data-equalizer-watch="deq-first">
        <div class="row">
          <div class="medium-6 col-contestmng-first column hide-for-small-only">
          </div>
          <div class="small-12 medium-6 col-contestmng-first column text-center">
          
            <img src="images/contest-mgr-logo.png">
          </div>
        </div>

        <div class="row">

          <div class="medium-3 col-contestmng-first column hide-for-small-only">
          </div>
         
          <div class="small-12 medium-9 col-contestmng-first column">
            <p id="pg-run"> RUN A</p>
            <p id="pg-contest">CONTEST</p>
            <p id="pg-connect">Connect with customers</p>
          </div>
          
        </div>
        
        <div class="row">
          <div class="medium-12 col-contestmng-first column text-center">
            <p class="pg-soliton">Soliton Apps</p>
          </div>
        </div>

        

      </div>
       <div class="medium-2 col-background column" data-equalizer-watch="deq-first">
        </div>
    </div> 
    </div> -->

   
  
   <div class="row max-width-row">
    <div class="medium-12 col-footer column text-center">
      <p class="footer-text">  Solitonapps.com/contestmanager    |    204.228.4785    |    info@solitonapps.com  </p>
    </div>
   </div>


   
   <div class="row max-width-row" data-equalizer="deq-second">
    <div class ="contain-to-grid">
        <div class="medium-2 col-background column" data-equalizer-watch="deq-second">
        </div>
     <div class="medium-3 col-background column" data-equalizer-watch="deq-second">
         <div class="row1">
            <div class="medium-12 column">
              <img src="images/Cedarwood-look.png">
            </div>
         </div>
         <div class="row2">
            <div class="medium-12 column">
              <img src="images/Seaco-4x8-night-dock.png">
            </div>
         </div>
         <div class="row3">
            <div class="medium-12 column">
              <p style="font-weight:bold">GREAT FOR:</p> 
              <ul style="list-style-type:disc">
                <li>TRADE SHOWS</li>
                <li>GOLF COURSES</li>
                <li>CORPORATE EVENTS</li>
              </ul>
            </div>
         </div>       
      </div>

     <div class="medium-6 col-background column end" data-equalizer-watch="deq-second">
        <div class="row">
          <div class="medium-12 col-cust-info column">
              <p id="pg-customer"> COLLECT CUSTOMER</p>
              <p id="pg-info">INFORMATION</p>
              <p id="pg-ipad">using your ipad</p>
          </div>
        </div>
        <div class="row">
           <div class="medium-2 col-list column">
           </div>
           <div class="medium-10 col-list column">
              <ul style="list-style-type:disc">
                  <li class="list-item">No data entry / No Paper</li>
                  <li class="list-item">Connect immediately</li>
                  <li class="list-item">Import into your database</li>
                  <li class="list-item">Engage customers well after initial meeting</li>
              </ul>
            </div>
           <!--  <div class="medium-2 column" style="margin-top:2em;">
           </div> -->
          </div>
          <div class="row">
            <div class="medium-12 col-soliton-second column">
                <p class="pg-soliton">Soliton Apps</p>
            </div>
          </div>

      </div>
      <div class="medium-1 col-background column" data-equalizer-watch="deq-second">
      </div>
    </div> <!-- Contain grid -->
   </div>

   <div class="row max-width-row">
    <div class="medium-12 col-footer column text-center">
      <p class="footer-text">  Solitonapps.com/contestmanager  |  204.228.4785  |  info@solitonapps.com  </p>
    </div>
   </div>
     
     



      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <script src="bower_components/foundation/js/foundation.min.js"></script>
      <script src="js/app.js"></script>
    </body>
    </html>
